# frozen_string_literal: true

module ResourceHelper
  def age(resource)
    age_for(resource, :created_at)
  end

  def closed_age(resource)
    age_for(resource, :closed_at)
  end

  def age_for(resource, field)
    (Date.today - Date.parse(resource[field])).to_i
  end
end

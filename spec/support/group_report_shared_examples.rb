# frozen_string_literal: true

RSpec.shared_context 'with report metadata' do
  let(:assignees) { ['@user1'] }
  let(:labels)    { %w[first-label second-label] }
  let(:mentions)  { ['@user2 @user3'] }
end

RSpec.shared_examples 'report summaries' do
  include_context 'with report metadata'

  describe 'labels' do
    it 'sets some default labels' do
      expect(subject).to match('/label ~"triage report" ~"type::ignore"')
    end

    it 'sets the provided labels' do
      expect(subject).to match('/label ~"first-label" ~"second-label"')
    end
  end

  context 'when assignees are present' do
    let(:assignees) { ['@user1'] }

    context 'when no mentions are present' do
      let(:mentions) { [] }

      it 'only includes assignees in the greeting' do
        expect(subject).to match("Hi `@user1`,\n")
      end

      it 'does not add a /cc command' do
        expect(subject).not_to match("/cc")
      end
    end

    context 'when mentions are present' do
      it 'includes assignees (backticked)' do
        expect(subject).to match("Hi `@user1`,\n")
      end

      it 'adds a /cc command with the mentions' do
        expect(subject).to match("/cc @user2 @user3\n")
      end
    end
  end
end

# frozen_string_literal: true

require_relative '../triage'
require_relative '../triage/job'
require_relative '../triage/reaction'
require_relative '../triage/markdown_table'
require_relative '../triage/result'
require_relative '../triage/scheduled_issue_validator'

module Triage
  class ScheduledIssueTypeLabelNudgerJob < Job
    include Reaction

    TYPE_LABEL_MISSING_MESSAGE = 'Please add ~"type::bug" ~"type::feature", ~"type::maintenance" and optionally a [subtype](https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification) label to this issue.'
    TYPE_IGNORE_FOOTER_MESSAGE = 'If you do not feel the purpose of this issue matches one of the types, you may apply the ~"type::ignore" label to exclude it from type tracking metrics and future prompts.'

    private

    def execute(event)
      prepare_executing_with(event)

      add_reminder if validator.type_label_nudge_needed?
    end

    def add_reminder
      comment = validator.type_label_unique_comment.wrap(type_label_reminder_text).strip
      add_comment(comment, append_source_link: true)
    end

    def validator
      @validator ||= ScheduledIssueValidator.new(event)
    end

    def last_user_changed_milestone
      @last_user_changed_milestone ||= latest_resource_milestone_event.to_h.dig('user', 'username')
    end

    def latest_resource_milestone_event
      @latest_resource_milestone_event ||=
        Triage.api_client.get(resource_milestone_events_path, query: { per_page: 1 }).last_page.last
    end

    def resource_milestone_events_path
      "/projects/#{event.project_id}/issues/#{event.iid}/resource_milestone_events"
    end

    def type_label_reminder_text
      result = Triage::Result.new
      result.errors << TYPE_LABEL_MISSING_MESSAGE

      mention_username = last_user_changed_milestone.nil? ? '' : "@#{last_user_changed_milestone}"

      <<~MARKDOWN.chomp
        :wave: #{mention_username} - please see the following guidance and update this issue.
        #{MarkdownTable.new(result).markdown}

        #{TYPE_IGNORE_FOOTER_MESSAGE}
      MARKDOWN
    end
  end
end
